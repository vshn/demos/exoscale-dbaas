#!/usr/bin/env bash

get_db_state() {
    exo dbaas show "$1" -z "$2" -O json | jq -r '.state'
}

wait_for_db() {
    echo "===> Waiting for DB '$1' in $2"
    EXPECTED="running"
    RESULT=$(get_db_state "$1" "$2")
    while [ "$RESULT" != "$EXPECTED" ]
    do
        echo "===> Not yet OK, sleeping 1 minute"
        sleep 60s
        RESULT=$(get_db_state "$1" "$2")
    done
    echo "===> DB '$1' in $2 OK"
}

DATABASE_NAME="wordpress"
REGION="ch-gva-2"

exo dbaas types list
exo dbaas create mysql hobbyist-1 $DATABASE_NAME -z $REGION
wait_for_db $DATABASE_NAME $REGION
