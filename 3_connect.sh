#!/usr/bin/env bash

DATABASE_NAME="wordpress"
REGION="ch-gva-2"

EXO_DB_JSON=$(exo dbaas show $DATABASE_NAME -z $REGION -O json)
EXO_DB_HOST=$(echo "$EXO_DB_JSON" | jq -r '.mysql.components[0].host')
EXO_DB_PORT=$(echo "$EXO_DB_JSON" | jq -r '.mysql.components[0].port')
EXO_DB_USERNAME=$(echo "$EXO_DB_JSON" | jq -r '.mysql.users[0].username')
EXO_DB_PASSWORD=$(echo "$EXO_DB_JSON" | jq -r '.mysql.users[0].password')

docker run -it --rm mysql mysql --host="$EXO_DB_HOST" --user="$EXO_DB_USERNAME" --password="$EXO_DB_PASSWORD" --port="$EXO_DB_PORT"
